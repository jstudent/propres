/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.uchicago.gerber;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ag
 */
public class UserBeanTest {
    
    public UserBeanTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getName method, of class UserBean.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        UserBean instance = new UserBean();
        String expResult = null;
        String result = instance.getName();
        assertEquals(expResult, result);

    }

    /**
     * Test of setName method, of class UserBean.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "Adam";
        UserBean instance = new UserBean();
        instance.setName(name);
        assertEquals(instance.getName(), name);

    }
    
}
